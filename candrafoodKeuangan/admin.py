from django.contrib import admin
from .models import Sales

# Register your models here.
@admin.register(Sales)
class SalesAdmin(admin.ModelAdmin):
    #pass
    list_display = [
        'nama',
        'tgl_lahir',
        'kota_lahir',
        'jenis_kelamin',
        'alamat',
        'no_telp',
        'no_rek'
    ]

    list_filter = [
        'nama',
        'kota_lahir',
        'alamat'
    ]

    search_fields = [
        'nama',
        'kota_lahir',
        'alamat'
    ]