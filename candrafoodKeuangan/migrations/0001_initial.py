# Generated by Django 3.1.4 on 2020-12-20 08:56

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Sales',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nama', models.CharField(max_length=255, verbose_name='nama')),
                ('tgl_lahir', models.DateField(verbose_name='tgl_lahir')),
                ('kota_lahir', models.CharField(max_length=255, verbose_name='kota_lahir')),
                ('jenis_kelamin', models.CharField(max_length=16, verbose_name='jenis_kelamin')),
                ('alamat', models.TextField(verbose_name='alamat')),
                ('no_telp', models.CharField(max_length=16, verbose_name='no_telp')),
                ('no_rek', models.CharField(max_length=32, verbose_name='no_rek')),
            ],
            options={
                'ordering': ['-id'],
            },
        ),
    ]
