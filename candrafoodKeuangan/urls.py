from django.urls import path
from .views import SalesCreateView, SalesDeleteView, SalesDetailView, SalesUpdateView, index, indexSales, indexWilayah, WilayahCreateView, WilayahDetailView, WilayahUpdateView, WilayahDeleteView, indexHarga, HargaCreateView, HargaUpdateView, HargaDetailView, HargaDeleteView

urlpatterns = [
    path('', index, name='home_page'),
    path('sales', indexSales, name='sales_index'),
    path('sales/insert', SalesCreateView.as_view(), name='sales_insert'),
    path('sales/<int:pk>', SalesDetailView.as_view(), name='sales_detail'),
    path('sales/edit/<int:pk>', SalesUpdateView.as_view(), name='sales_update'),
    path('sales/delete/<int:pk>', SalesDeleteView, name='sales_delete'),
    path('wilayah', indexWilayah, name='wilayah_index'),
    # d
    path('wilayah/insert', WilayahCreateView.as_view(), name='wilayah_insert'),
    path('wilayah/detail/<int:pk>', WilayahDetailView.as_view(), name='wilayah_detail'),
    path('wilayah/edit/<int:pk>', WilayahUpdateView.as_view(), name='wilayah_update'),
    path('wilayah/delete/<int:pk>', WilayahDeleteView, name='wilayah_delete'),
    path('harga', indexHarga, name='harga_index'),
    path('harga/insert', HargaCreateView.as_view(), name='harga_insert'),
    path('harga/detail/<int:pk>', HargaDetailView.as_view(), name='harga_detail'),
    path('harga/edit/<int:pk>', HargaUpdateView.as_view(), name='harga_update'),
    path('harga/delete/<int:pk>', HargaDeleteView, name='harga_delete'),
]