from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse

# Create your models here.
class Sales(models.Model):
    nama            = models.CharField('nama', max_length=255)
    tgl_lahir       = models.DateField('tgl_lahir')
    kota_lahir      = models.CharField('kota_lahir', max_length=255 )
    jenis_kelamin   = models.CharField('jenis_kelamin', max_length=16)
    alamat          = models.TextField('alamat')
    no_telp         = models.CharField('no_telp', max_length=16)
    no_rek          = models.CharField('no_rek', max_length=32)

    class Meta:
        ordering = ['-id']
    
    def __str__(self):
        return self.nama

    def get_absolute_url(self):
        return reverse('sales_index')


# --------------  Wilayah  ---------------- #
class Wilayah(models.Model):
    kode_wilayah = models.CharField('kode_wilayah', max_length=255)
    nama_wilayah = models.CharField('nama_wilayah', max_length=255)

    class Meta:
        ordering = ['-id']
    
    def __str__(self):
        return self.nama_wilayah
    def get_absolute_url(self):
        return reverse('wilayah_index')

class Hargas(models.Model):
    nama_brg = models.CharField('nama_brg', max_length=255)
    kode_brg = models.CharField('kode_brg', max_length=196)
    jenis_brg = models.CharField('jenis_brg', max_length=64)
    qty1 = models.IntegerField('qty1')
    qty2 = models.IntegerField('qty2')
    satuan1 = models.CharField('satuan1', max_length=16)
    satuan2 = models.CharField('satuan2', max_length=16)
    harga1 = models.CharField('harga1', max_length=64)
    harga2 = models.CharField('harga2', max_length=64)

    class Meta:
        ordering = ['-id']
    
    def __str__(self):
        return self.nama_brg
    def get_absolute_url(self):
        return reverse('harga_index')
