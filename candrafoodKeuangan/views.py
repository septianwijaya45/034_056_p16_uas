from django.db.models import fields
from django.http import request
from django.urls import reverse_lazy
from candrafoodKeuangan.models import Sales, Wilayah, Hargas
from django.shortcuts import redirect, render
from django.views.generic import DetailView, CreateView, UpdateView, DeleteView

# Create your views here.
var = {}
# -------- Dashboard --------- #
def index(self):
    sales= Sales.objects.all()
    wilayah = Wilayah.objects.all()
    return render(self, 'dashboard/index.html', {'sales':sales, 'wilayah':wilayah})

# -------- Sales --------- #
def indexSales(self):
    sales= Sales.objects.all()
    return render(self, 'sales/index.html', {'sales':sales})


class SalesCreateView(CreateView):
    model = Sales
    fields = '__all__'
    template_name = "sales/insert.html"
    
    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context


class SalesDetailView(DetailView):
    model = Sales
    template_name = "sales/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class SalesUpdateView(UpdateView):
    model = Sales
    fields = [
        'nama',
        'tgl_lahir',
        'kota_lahir',
        'jenis_kelamin',
        'alamat',
        'no_telp',
        'no_rek'
    ]
    template_name = "sales/edit.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


def SalesDeleteView(self, pk):
    sales = Sales.objects.get(id = pk)
    sales.delete()
    return redirect('sales_index')


# ------------  Wilayah   ------------- #
def indexWilayah(self):
    wilayah = Wilayah.objects.all()
    return render(self, 'wilayah/index.html', {'wilayah':wilayah})

# d
class WilayahCreateView(CreateView):
    model = Wilayah
    fields = '__all__'
    template_name = "wilayah/insert.html"

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class WilayahDetailView(DetailView):
    model = Wilayah
    template_name = "wilayah/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class WilayahUpdateView(UpdateView):
    model = Wilayah
    fields = [
        'kode_wilayah',
        'nama_wilayah'
    ]
    template_name = 'wilayah/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

def WilayahDeleteView(self, pk):
    wilayah = Wilayah.objects.get(id = pk)
    wilayah.delete()
    return redirect('wilayah_index')

# ------ Harga ------- #
def indexHarga(self):
    harga = Hargas.objects.all()
    return render(self, 'harga/index.html', {'harga':harga})

class HargaCreateView(CreateView):
    model = Hargas
    fields = '__all__'
    template_name = "harga/insert.html"

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class HargaDetailView(DetailView):
    model = Hargas
    template_name = "harga/detail.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

class HargaUpdateView(UpdateView):
    model = Hargas
    fields = [
        'nama_brg',
        'kode_brg',
        'jenis_brg',
        'qty1',
        'qty2',
        'satuan1',
        'satuan2',
        'harga1',
        'harga2'
    ]
    template_name = 'harga/edit.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context

def HargaDeleteView(self, pk):
    harga = Hargas.objects.get(id = pk)
    harga.delete()
    return redirect('harga_index')